<?php

namespace Yolo\Utils;

use Closure;

class Number
{
    /**
     * 创建一个代码索引函数，它总是返回前一个整数的下一个较大的整数。
     * @param int $base 基数
     * @return Closure
     */
    public static function createIncreaseCodeIndex(int $base): Closure
    {
        return function() use (&$base)
        {
            return ++$base;
        };
    }

    /**
     * 创建一个代码索引函数，它总是返回前一个整数的上一个较小的整数。
     * @param int $base 基数
     * @return Closure
     */
    public static function createDecreaseCodeIndex(int $base): Closure
    {
        return function() use (&$base)
        {
            return --$base;
        };
    }
}