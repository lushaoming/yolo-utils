<?php

namespace Yolo\Utils;

use Exception;

class Errors
{
    /**
     * 将异常对象转换成数组
     * @param Exception $e
     * @return array
     */
    public static function errorToArray(Exception $e): array
    {
        return [
            'code' =>$e->getCode(),
            'message' =>$e->getMessage(),
            'traces' => $e->getTrace(),
        ];
    }
}
