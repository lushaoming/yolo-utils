<?php

namespace Yolo\Utils;

use Hyperf\Stringable\Str;

class Path
{
    /**
     * 格式化路径（前后加上/）
     * @param string $path 路径
     * @return string
     */
    public static function formatPath(string $path): string
    {
        if (!Str::startsWith($path, '/')) {
            $path = '/' . $path;
        }

        if (!Str::endsWith($path, '/')) {
            $path = $path . '/';
        }

        return $path;
    }
    /**
     * 根据接口类型获取控制器路径
     * @param string $type 接口类型
     * @return string 控制器路径
     */
    public static function getControllerAbsolutePath(string $type): string
    {
        return BASE_PATH . "/app/Controller/$type";
    }

    /**
     * 解析restful的path
     * @param string $path
     * @return array
     */
    public static function parseUri(string $path): array
    {
        $pattern ='/\{([^}]+)}/';

        $path = trim($path, '/');

        $paths = explode('/', $path);

        $type = 'static';

        $variables = [];
        foreach ($paths as $item) {
            if ($item === '') {
                continue;
            }

            if (preg_match('/^\{([^}]+)}$/', $item, $matches)) {

                $variables[] = $matches[1];
                $type = 'pattern';
            }
        }

        if ($type === 'pattern') {
            $handled = preg_replace($pattern, '[^/]*', $path);
            $pathPattern = '/^\/'.str_replace('/', '\/', $handled).'\/$/';
        }

        return [
            'type' => $type,
            'pattern' => $pathPattern ?? '',
            'variables' => $variables,
        ];
    }
}
