<?php

namespace Yolo\Utils;

use Godruoyi\Snowflake\Snowflake;

class Uuid
{
    /**
     * 生成UUID
     * @param int $datacenterId center ID
     * @param int $workerId machine ID
     * @return string
     */
    public static function generateUUID(int $datacenterId, int $workerId): string
    {
        $snowflake = new Snowflake($datacenterId, $workerId);

        return $snowflake->id();
    }
}
