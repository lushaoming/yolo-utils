<?php

namespace Yolo\Utils;

use function Hyperf\Support\env;

class System
{
    /**
     * 获取服务的worker数量
     * @return int
     */
    public static function getWorkerNum(): int
    {
        return (int)env('SERVER_WORKER_NUM', 1);
    }
}
